<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Document</title>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> 
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.66.0-2013.10.09/jquery.blockUI.js"></script> 
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
        
    </head>
    <body>
        <style>
            
            .tables {
                position: absolute;
                left: 25%;
            }
            table {
                margin-bottom: 10px;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                text-align: left;
            }
            
            .asset_results{
               
            }
            th, td {
                padding: 15px;

            }

            .success {
                background-color: #EFE;
            }
            .failed {
                background-color: #FFEFEF;
            }
            .hide {
                display: none;
            }
        </style>
   
        <div class="tables">

            <?php

            require_once('url_scraper_source.php');

            foreach($subdomains_array as $key => $subdomain_specific_array)
            {
                echo "<pre> <br>";
                echo "<h1>$key:</h1>";
                echo "</pre><br>";
                echo "<form>";
                echo "<table><tr><th style=\"text-align:center;\">check all <br><input type=\"checkbox\" subdomain=\"$key\" class=\"checkbox\"/></th><th>Url</th><th>Status</th></tr>";
                foreach($subdomain_specific_array as $k => $url)
                {
                    echo "<tr class=\"table-urls $key row_$k\"><td style=\"text-align:center;\"><input type=\"checkbox\" class=\"$key" . "_box row_". $k ."\" name=\"url\" subdomain=\"$key\" value=\"\" row=\"$k\" url=\"$url\"/></td><td>$url</td><td subdomain=\"$key\" row=\"$k\" class=\"status status_$k\"></td></tr>";
                    echo "<tr><td colspan=\"3\" class=\"asset_results hide $key append_$k\"></td></tr>";
                }
                echo "<tr><th style=\"text-align:center;\">check all <br><input type=\"checkbox\" subdomain=\"$key\" class=\"checkbox\"/></th><th></th><th></th></tr>";
                echo "</table>";
                echo "<br><button subdomain=\"$key\" class=\"url-value-retrieve\">Save to File: $key </button><span  class=\"time_stamp_$key\"></span><br>";
                echo "</form>";
            }

            ?>
    
    
        </div>
<script type="text/javascript">

    function prepareLayer($n) {
        return $.get('./a.html').then(function(data) {
            $n.html(data);
        });
    }
    
    function postPreparation(subdomain, functionStart, functionEnd) {
        $('.time_stamp_' + subdomain).html('  <i class="fa fa-arrow-right"></i> Done ( ' + (functionEnd - functionStart) + ' ) ms');
    }
    $(function(){
      
  
    // Listen for click on toggle checkbox
    $('.checkbox').click(function(event) {
        var subdomain = $(this).attr('subdomain');
        if(this.checked) {
            // Iterate each checkbox
            $('.'+subdomain+' :checkbox').each(function() {
                this.checked = true;
                var rowNumber = $(this).attr('row');
                $('.'+subdomain+' .status_' + rowNumber).html('Queued');
                
            });
        }
        else {
            // Iterate each checkbox
            $('.'+subdomain+' :checkbox').each(function() {
                this.checked = false;
                var rowNumber = $(this).attr('row');
                $('.'+subdomain+' .status_' + rowNumber).html('');
            });
        }
    });
    
    $(':checkbox').click(function() {
        var subdomain = $(this).attr('subdomain');
        var rowNumber = $(this).attr('row');
        if(this.checked) {
            $('.'+subdomain+' .status_' + rowNumber).html('Queued');
        }
        else {
            $('.'+subdomain+' .status_' + rowNumber).html('');
        }
    });
    
    $('.status').click(function(){
        var rowNumber = $(this).attr('row');
        var subdomain = $(this).attr('subdomain');
        $('.'+subdomain+'.asset_results.append_'+rowNumber).toggleClass('hide');
    });
    
    
    //whatever checkbox is selected, grab that value and pass it 
    //into ajax to send to new_and_imporved_scraper.php
    //run one url, while it is processing, change the status to processing
    //pass back whether it succeeded or failed and update the status. 
    //run the urls sync
    
    $(".url-value-retrieve").click(function(event){
        event.preventDefault();
        
        var subdomain = $(this).attr('subdomain');
        $('.time_stamp_' + subdomain).html('  <i class="fa fa-arrow-right"></i> Saving...');
        var functionStart = performance.now();
       var i = 0;
        var checkboxArray = $('.table-urls input:checkbox:checked');
        checkboxArray.each(function(index){
            i += 1;
            var rowNumber = $(this).attr('row');
            var url = $(this).attr('url');
            var subdomain = $(this).attr('subdomain');
            var phpData = [
                {key: 'subdomain', val: subdomain},
                {key: 'url', val: url}
            ];
            
//            $('input.'+subdomain+'_box.row_'+rowNumber)
            $('.'+subdomain+' .status_' + rowNumber).html('Pending...');
            $.ajax({
                url: 'scraper_respond_to_ajax.php',
                type: 'post',
                data: {data: phpData},
                dataType: "html",
                success: function(data) {
                    var dataObject = JSON.parse(data);
                    
                    
                    if(dataObject.status === 'success') {
//                        console.log(dataObject);
                        $('.'+subdomain+' .status_' + rowNumber).html('Success');
                        $('.'+subdomain+'.row_' + rowNumber).addClass('success');
                        $.each(dataObject, function(i, value){
                            if(i === 'status'){
                                return;
                            }
//                            console.log(i);
                            $('.'+subdomain+'.asset_results.append_'+rowNumber).append(i+"<br>");
//                            console.log(value);
                            $.each(value, function(key, val){
//                                console.log(val);
                                $('.'+subdomain+'.asset_results.append_'+rowNumber).append(val+"<br>");
                            });
                        });
                    }
//                
                    else if (dataObject.status === 'failed') {
                        console.log(dataObject);
                        $('.'+subdomain+' .status_' + rowNumber).html('Failed');
                        $('.'+subdomain+'.row_' + rowNumber).addClass('failed');
                    }
                },
                complete: function() { 
                    if(i === checkboxArray.length) {
                        var functionEnd = performance.now();
                        $('.time_stamp_' + subdomain).html('  <i class="fa fa-arrow-right"></i> Done ( ' + (functionEnd - functionStart) + ' ) ms');
                    }
                    
            }
            this.checked = false; 
            
        });
        
        
//        $('.time_stamp_' + subdomain).html('  <i class="fa fa-arrow-right"></i> Done ( ' + (functionEnd - functionStart) + ' ) ms');
    });
    
  });
  
  });
</script>

    </body>


</html>