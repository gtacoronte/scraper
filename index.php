<?php

require_once('url_scraper_source.php');

/************************************************************************
 * myCurl Function
 *************************************************************************/
/**
 * Pass in a url
 * pass in true if you want to check to see if the url will return a 404. else 
 * if it is an asset to a page, pass in false
 * myCurl will initialize cURL 
 * Set cURL's options using the previously assigned array data in $options
 * Execute the cURL request and assign the returned data to the $data variable
 * Then close cURL
 * 
 * @param string $url
 * @return string
 */

function myCurl($url, $status_check=false) {
    
    // Assigning cURL options to an array
    $options = [CURLOPT_RETURNTRANSFER => TRUE,
        
        // Setting cURL to follow 'location' HTTP headers
        CURLOPT_FOLLOWLOCATION => TRUE,
        
        // Automatically set the referer where following 'location' HTTP headers
        CURLOPT_AUTOREFERER => TRUE, 
        
        // Automatically set the referer where following 'location' HTTP headers
        CURLOPT_CONNECTTIMEOUT => 320,
        
        // Setting the maximum amount of time for cURL to execute queries
        CURLOPT_TIMEOUT => 320,
        // Setting the maximum number of redirections to follow
        CURLOPT_MAXREDIRS => 20,
        
        // Setting the useragent
        CURLOPT_USERAGENT => "Mozilla/5.0 (X11; U; Linux i686; en-US;"
        . "rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",
        
        // Setting cURL's URL option with the $url variable passed into the function
        CURLOPT_URL => $url, 
        ];
    
    // Initializing cURL
    $ch = curl_init();
    
    // Setting cURL's options using the previously assigned array data in $options
    curl_setopt_array($ch, $options);
    
    // Executing the cURL request and assigning the returned data to the $data variable
    $data = curl_exec($ch);
    
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
    // Closing cURL
    curl_close($ch);
    if($status_check)
    {
        if($http_status === 200)
        {
            // Returning the data from the function
            return $data;

        }
    }
    else
    {
        return $data;     
       
    }
}

/************************************************************************
 * scrape_between Function
 *************************************************************************/

/**
 * Strips all data from before $start pos
 * Then strips the start
 * Gets the position of the $end of $data to scrape
 * Strips all data from after and including the $end of $data, to scrape
 * Returns the scraped data from the function
 * 
 * @param string $data
 * @param string $start
 * @param string $end
 * @return string
 */

function scrape_between($data, $start, $end) 
{
    
    $data = stristr($data, $start);
    
    $data = substr($data, strlen($start));

    $stop = stripos($data, $end);
    
    $data = substr($data, 0, $stop);
    
    return $data;
}

$results_page = '';
foreach($subdomain_specific_array as $key => $client_url)
{
    
    $subdomain_specific_dir = '/opt/bitnami/apache2/' . $key;
    
     /**
     * If creating a new directory and initializing a git repository:
     * give chmod 777 on the folder you are writing to. ex: scrape_data
     * after git init in scrape_data,  use chmod o+rw -R scrape_data;
     * This will allow apache to process the php exec commands for git with no permissions problem.
     */
    //create new directory with 744 permissions if it does not exist
    if ( !is_dir($subdomain_specific_dir) ) 
    {
        mkdir ($subdomain_specific_dir, 0777);
        chmod($subdomain_specific_dir, 0777);  
        chdir($subdomain_specific_dir);
        exec("git init");
    }
    foreach($client_url as $url)
    {
       
        if(strpos($url, 'http') !== 0) 
        {   
            $url = 'http://' . $url;
        }
        
        //Passing url, input by user, into the function myCurl()
        $status = true;
        $results_page = myCurl($url, $status);

        if($results_page == '')
        {
            continue;
        }
        
        $url .= '.html';
        $client_url_to_parse = parse_url($url);
        $parsed_client_url = explode('/',"{$client_url_to_parse['path']}");
       
        /***************************************************************************************/
        $full_client_url_path = $subdomain_specific_dir;
        $url_path = '';
        foreach($parsed_client_url as $client_url_folder_path)
        {
//            if($client_url_folder_path == '')
//            {
//                continue;
//            }
//            if(strpos($client_url_folder_path, 'revolutiongolf.com') !== false)
//            {
//                continue;
//            }
            if(strpos($client_url_folder_path, '.') === false)
            {
                //these should all only be folders in the path
                //build path

                $url_path .= $client_url_folder_path;

                $full_client_url_path = $subdomain_specific_dir . '/' . $url_path;

                //check to see if directory exists in path, if not, make it
                if(!is_dir($full_client_url_path))
                {
                    //create the directory
                    mkdir ($full_client_url_path, 0777);
                }   
            }
            else
            {
                
                echo "<pre> file to save to: <br>";
                echo $full_client_url_path . $client_url_folder_path;
                echo "<br>Line: " . __LINE__ . " in " . __FILE__;
                echo "</pre><br>";
                
                
                //save file here
                $fp = fopen($full_client_url_path . $client_url_folder_path, 'w');
                fwrite($fp,$results_page);
                fclose($fp);
                
            }
        }

        
        if(substr($full_client_url_path, -1) ==='/')
        {
            $full_client_url_path = substr($full_client_url_path, 0, -1);
        }
        /****************************************************************************************/
        
        //get .html page name by grabbing the last dir in $url
//        $parse_for_page_name = strrpos($url, '/');
//        $html_page_name = substr($url, $parse_for_page_name);               
//        
//        $fp = fopen($subdomain_specific_dir . $html_page_name, 'w');
//        fwrite($fp,$results_page);
//        fclose($fp);
//      
        //Get the homepage of the url to later prepend to links (images, css, js)
        //returns a string
        //change variable name to host_url
        $url_explode = scrape_between($url, "//", "/");

        // Scraping out only the middle section of the results page
        $results_data = scrape_between($results_page, "<html", "</html>");

        $my_first_regex = '/(<script|<img)[\s\w"\'\=\/\-\:\;]+src=["\']([^"\']+)["\']|<link[\s\w"\'\=\/]+href=["\']([^"\']+)["\']|url\(["\']?([^"\'\)]+)["\'\)]?/';
        /*
         * preg_match_array(
            * 0 = all matches
            * 1 = bs
            * 2 = src of scripts and images
            * 3 = src of css
            * 4 = images in css
         * )
         * 
         * $1 bs
         * $2 src of scripts and images
         * $3 src of css
         * $4 images in css
         * 
         */

        preg_match_all($my_first_regex, $results_data, $separate_results);
       
        foreach($separate_results as $key => $separate_result) 
        {    
            if ($key === 0 || $key === 1)
            {
                continue;
            }
            //foreach one in the list, get url
            foreach($separate_result as $result)
            {

                if($result === "")
                {
                    continue;
                }

                $result_url = $result; 
                
                //if not http://, add it
                if(strpos($result, 'http') !== 0) 
                {   
                    $result_url =  'http:' . $result;

                    if(strpos($result, '//') !== 0) 
                    { 
                        $result_url = 'http://' . $url_explode . $result;
                    }
                }
               
                //get asset (curl item)
                $result_data = myCurl($result_url);

                //save to file
                //remove http://domain.tdl

                $var = parse_url($result_url);
                
                //break relative url on / (get folders and file)

                    
                    if(array_key_exists('path', $var))
                    {
                        $dir_path = explode('/',"{$var['path']}");
                    }
                    else
                    {
                        $dir_path = explode('/',"{$var['host']}");
                    }

                $full_path = $full_client_url_path;
                foreach($dir_path as $key=>$dir)
                {
                    
                    if($dir === '')
                    {
                            continue;
                    }
                    elseif(strpos($dir, '.js') ===false && strpos($dir, '.css') ===false && strpos($dir, '.png') ===false && strpos($dir, '.jpg') ===false || strpos($dir, '..') === 0)
                    {
                        //these should all only be folders in the path
                        //build path
                        $path = '';

                        //foreach folder (top down)
                        foreach($dir_path as $k=>$d)
                        {

                            if($k <= $key)
                            {

                                if($d === '..')
                                {
                                    //remove the previous directory
                                    $ok = $k - 1;
                                    echo "<br />Excuting .. exception: <pre>";
                                    echo "Line: " . __LINE__ . " in " . __FILE__;
                                    echo "</pre>";

                                    $remo = $dir_path[$ok] . "/";
                                    $full_path = str_replace($remo, '',$full_path);

                                    continue;
                                }

                                $path .= $d."/";

                                $full_path = $full_client_url_path . $path;

                                //check to see if directory exists in path, if not, make it
                                if(!is_dir($full_path))
                                {
                                    //create the directory
                                    mkdir ($full_path, 0777);
                                }   
                            }

                        }
                        echo "<pre> <br>";
                        echo "path made: $full_path  <br>";
                        echo "Line: " . __LINE__ . " in " . __FILE__;
                        echo "</pre> <br>";
                    }
                    else
                    {
                        if(substr($full_path, -1) ==='/')
                        {
                            $full_path = substr($full_path, 0, -1);
                        }
                        //we are expecting this to only be files that get here
                        //save file to correct local path   
                        $f = fopen($full_path . '/' . $dir , "w");
                        fwrite($f, $result_data);
                        fclose($f);

                        echo "<pre><br>";
                        echo "file made" . $full_path . '/' . $dir . "<br>";
                        echo "Line: " . __LINE__ . " in " . __FILE__;
                        echo "</pre><br>";

                        //if is css file, parse for images
                        if(strpos($dir, '.css')) 
                        {
                            preg_match_all($my_first_regex, $result_data, $css_images);

                            foreach($css_images[4] as $image)
                            {
                                $image_url = $image;
                                
                                if (strpos($image, 'http') !== 0)
                                {
                                    if(strpos($image, '/') !== 0 && strpos($image, '//') !== 0 && strpos($image, '../') !== 0)
                                    {
                                        continue;
                                    }
                                    $image_url = 'http:' . $image;

                                    if (strpos($image, '../') === 0)
                                    {
                                        $image_url = 'http://' . $url_explode . $path . $image;
                                    }
                                    else if (strpos($image, '//') !== 0)
                                    {
                                        $image_url = 'http://' . $url_explode . $image;
                                    }
                                }
                                //Expect all images that have http:// and the domain prepended to it
                                $css_image = myCurl($image_url);
                                $parsed_image_path = parse_url($image_url);

                                //break relative url on / (get folders and file)
                                $image_dir_path = explode('/',"{$parsed_image_path['path']}");

                                $full_path = $full_client_url_path;
                                $appended_image_dir_path = '';
                                foreach($image_dir_path as $jk=>$image_path)
                                {
                                    if($image_path === '')
                                    {
                                        continue;
                                    }
                                    elseif(strpos($image_path, '.js') ===false && strpos($image_path, '.css') ===false && strpos($image_path, '.png') ===false && strpos($image_path, '.jpg') ===false || strpos($image_path, '..') === 0)
                                    {
                                        //these should all only be folders in the path
                                        //build path

                                        if($image_path === '..')
                                        {
                                            //remove the previous directory
                                            $old_key = $jk - 1;
                                            echo "<br />Excuting .. exception: <pre>";
                                            echo "Line: " . __LINE__ . " in " . __FILE__;
                                            echo "</pre>";

                                            $remove = $image_dir_path[$old_key] . "/";
                                            $appended_image_dir_path = str_replace($remove, '',$appended_image_dir_path);

                                            continue;
                                        }

                                        $appended_image_dir_path .= $image_path . "/";

                                        $full_path = $full_client_url_path . '/' . $appended_image_dir_path;

                                        //check to see if directory exists in path, if not, make it
                                        if(!is_dir($full_path))
                                        {
                                            //create the directory
                                            mkdir ($full_path, 0777);
                                        }   
                                    }
                                    else
                                    {
                                        
                                        if(substr($full_path, -1) ==='/')
                                        {
                                            $full_path = substr($full_path, 0, -1);
                                        }
                        
                                        echo "<pre><br /> saving image... <br />";
                                        echo $full_path . '/' . $image_path;
                                        echo "Line: " . __LINE__ . " in " . __FILE__;
                                        echo "</pre><br>";
                                        
                                        //we are expecting this to only be files that get here
                                        //save file to correct local path   
                                        $s = fopen($full_path . '/' . $image_path, "w");
                                        fwrite($s, $css_image);
                                        fclose($s);
                                        echo " <br/> image saved <br/>";
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

   
     /**
     * If creating a new directory and initializing a git repository:
     * give chmod 777 on the folder you are writing to. ex: scrape_data
     * after git init in scrape_data,  use chmod o+rw -R scrape_data;
     * This will allow apache to process the php exec commands for git with no permissions problem.
     */
    //fix to where it will git init on every repo if it was just created, then it add and commit for every repo it updated
    chdir($subdomain_specific_dir);
    exec("git add -A");  
    exec("git commit -m'Commiting changes. -your friendly auto scraper bot'");
}
