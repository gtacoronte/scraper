<?php

/**
 * Pass in a url
 * pass in true if you want to check to see if the url will return a 404. else 
 * if it is an asset to a page, pass in false
 * myCurl will initialize cURL 
 * Set cURL's options using the previously assigned array data in $options
 * Execute the cURL request and assign the returned data to the $data variable
 * Then close cURL
 * 
 * @param string $url
 * @return string
 */

function myCurl($url, $status_check=false) {
    
    // Assigning cURL options to an array
    $options = [CURLOPT_RETURNTRANSFER => TRUE,
        
        // Setting cURL to follow 'location' HTTP headers
        CURLOPT_FOLLOWLOCATION => TRUE,
        
        // Automatically set the referer where following 'location' HTTP headers
        CURLOPT_AUTOREFERER => TRUE, 
        
        // Automatically set the referer where following 'location' HTTP headers
        CURLOPT_CONNECTTIMEOUT => 320,
        
        // Setting the maximum amount of time for cURL to execute queries
        CURLOPT_TIMEOUT => 320,
        // Setting the maximum number of redirections to follow
        CURLOPT_MAXREDIRS => 20,
        
        // Setting the useragent
        CURLOPT_USERAGENT => "Mozilla/5.0 (X11; U; Linux i686; en-US;"
        . "rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",
        
        // Setting cURL's URL option with the $url variable passed into the function
        CURLOPT_URL => $url, 
        ];
    
    // Initializing cURL
    $ch = curl_init();
    
    // Setting cURL's options using the previously assigned array data in $options
    curl_setopt_array($ch, $options);
    
    // Executing the cURL request and assigning the returned data to the $data variable
    $data = curl_exec($ch);
    
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
    // Closing cURL
    curl_close($ch);
    if($status_check == 1)
    {
        if($http_status != false && $http_status != 0 && $http_status != 404)
        {
            // Returning the data from the function
            return $data;
        }
    }
    else
    {
        return $data;     
    }
}

/**
 * Strips all data from before $start pos
 * Then strips the start
 * Gets the position of the $end of $data to scrape
 * Strips all data from after and including the $end of $data, to scrape
 * Returns the scraped data from the function
 * 
 * @param string $data
 * @param string $start
 * @param string $end
 * @return string
 */

function scrape_between($data, $start, $end) 
{
    
    $data = stristr($data, $start);
    
    $data = substr($data, strlen($start));

    $stop = stripos($data, $end);
    
    $data = substr($data, 0, $stop);
    
    return $data;
}

/**
 * when passing the array from google analytics, to ga_urls, 
 * it is the pages['rows'] that gets passed through
 * create an associative array with the key being the subdomain and 
 * the value being an indexed array of the urls under that subdomain
 * @param array $url_array
 * @return assoc. array
 */

function ga_urls($url_array){
    
    $subdomains_array = [];
    //run through each url returned from google
    foreach($url_array as $key => $url)
    {
        if($key ===0 || $key ===1) 
        {
            continue;
        }
        $parsed_client_list = parse_url($url[0]);

        $page_array_to_string = $parsed_client_list['path'];

        $rg_pos = strpos($page_array_to_string, 'revolutiongolf.com');

        //string position starting at .revolutiongolf.com 
        if($rg_pos === false )
        {
           continue;
        }

        $subdomain = substr($page_array_to_string, 0, $rg_pos -1);

        if(array_key_exists($subdomain, $subdomains_array))
        {
            if(in_array($page_array_to_string, $subdomains_array[$subdomain]))
            {
               continue;        
            }
        }
        $subdomain_focus = ['tourstrikertraining', 'mcleangolf', 'coaches'];
        if(in_array($subdomain, $subdomain_focus))
        {
            $subdomains_array[$subdomain][] = $page_array_to_string;
        }
    }
    return $subdomains_array;
}

/**
 * receives $url from foreach loop 
 * if it does not have http, prepend it
 * cURL the url and retreive the page data
 * parse the url and cature the path
 * 
 * @param string $url
 */
function create_host_dir($url, $subdomain_specific_dir) {
    $http_url = $url;
    if(strpos($url, 'http') !== 0)
    {
        $http_url = 'http:' . $url;

        if(strpos($url, '//') !== 0) 
        { 
            $http_url = 'http://' . $url;
        }
    }
    //Passing url, input by user, into the function myCurl()
    $status = true;
    
    //retrieve the index page of url and save to file in subdomain directory
    $results_page = myCurl($http_url, $status);
    
    if($results_page != '')
    {
        
        $http_url .= '.html';
        $parsed_url = parse_url($http_url);
        $directory_path = explode('/',"{$parsed_url['path']}");
        $return_array = create_dir_structure($directory_path, $results_page, $subdomain_specific_dir);
        $results_array = [$results_page, $return_array[0], $http_url];
        return $results_array;
    }
    else 
    {
        $GLOBALS['ajax_return_array']['status'] = 'failed';
        $ajax_return_array = json_encode($GLOBALS['ajax_return_array']);
        print_r($ajax_return_array);
        die();
    }
    
}

/**
 * $directory_path is an array of the url path to create the dir structure after.
 * $results_page is the scraped paged, passed to create the file.
 * $subdomain_specific_dir is the directory path to save under.
 * returns the $full_path in which it was saved - the file name
 * if it is a css file it will return an array of 
 * @param array $directory_path
 * @param string $results_page
 * @param string $subdomain_specific_dir
 * @return string (or if .css file, array)
 */
function create_dir_structure($directory_path, $results_page, $subdomain_specific_dir){
    
    $last_index = count($directory_path) - 1;
    $full_path = $subdomain_specific_dir;
    $path = '';
    
    foreach($directory_path as $key => $directory)
    {
        if($directory === '' || strpos($directory, '.com') !== false)
        {
                continue;
        }
        elseif($key < $last_index)
        {
            if(substr($path, -1) !=='/')
            {
                $path .= '/';
            }
            //these should all only be folders in the path
            //build path
            

            //foreach folder (top down)
            
            if($directory === '..')
            {
                //remove the previous directory
                $ok = $key - 1;

                $remo = $directory_path[$ok] . "/";
                $full_path = str_replace($remo, '',$full_path);

                continue;
            }

            $path .= $directory."/";

            $full_path = $subdomain_specific_dir . $path;

            //check to see if directory exists in path, if not, make it
            if(!is_dir($full_path))
            {
                //create the directory
                mkdir ($full_path, 0777);
            }
        }
        else
        {
            if(strpos($directory, '.') !== false)
            {
                //call to file creation function
                save_asset_to_file($full_path, $directory, $results_page);
                
                if(strpos($directory, '.css') !== false)
                {
                    $file_path = $full_path . $directory;
                    $results_array = [$file_path, $results_page, $path];
                    return $results_array;
                }
            }
        }
    }
    //remove the trailing / if there is one.
    if(substr($full_path, -1) ==='/')
    {
        $full_path = substr($full_path, 0, -1);
    }
    $return_array = [$full_path];
    return $return_array;
}

/**
 * 
 * @param string $full_path
 * @param string $directory
 * @param string $results_page
 */
function save_asset_to_file($full_path, $directory, $results_page){
    
    //we are expecting this to only be files that get here
    
    //remove the trailing / if there is one.
    if(substr($full_path, -1) ==='/')
    {
        $full_path = substr($full_path, 0, -1);
    }
    $file_extention_start = strrpos($directory, '.');
    
    $file_extention = substr($directory, $file_extention_start);
    
    $recorded_path = $full_path . '/' . $directory;
    
    $GLOBALS['ajax_return_array'][$file_extention][] = $recorded_path;
    $GLOBALS['ajax_return_array']['status'] = 'success';
    $f = fopen($full_path . '/' . $directory , "w");
    fwrite($f, $results_page);
    fclose($f);
    
}

/**
 * 
 * @param type $results_page
 * @return array
 */
function results_page_regex($results_page){
    $my_first_regex = 
    '/(<script|<img)[\s\w"\'\=\/\-\:\;]+src=["\']([^"\']+)["\']|<link[\s\w'
    . '"\'\=\/]+href=["\']([^"\']+)["\']|url\(["\']?([^"\'\)]+)["\'\)]?/';
    /*
     * $1 all results
     * $2 src of scripts and images
     * $3 src of css
     * $4 images in css
     */

    preg_match_all($my_first_regex, $results_page, $separate_results);
   
    return $separate_results;
}
/**
 * pass in url 
 * if it has http:// return that value
 * if it has // add http:
 * if it does not have any scheme, host or subdomain, add them and return the http_url
 * 
 * @param string $url
 * @param string $url_explode (optional)
 * @return string $http_url
 */
function add_http($url, $url_explode='', $path=''){
    
    //if not http://, add it
    $http_url = $url;
    
    
    if(strpos($url, 'http') !== 0) 
    {   
        if(strpos($url, '/') === 0 || strpos($url, '//') === 0 || strpos($url, '../') === 0)
        {
            $http_url =  'http:' . $url;
            if (strpos($url, '../') === 0)
            {
                $http_url = 'http://' . $url_explode . $path . $url;
            }
            else if(strpos($url, '//') !== 0) 
            { 
                $http_url = 'http://' . $url_explode . $url;
            }
        }
    }
  
    return $http_url;
}

/**
 * cURLs the url passed in
 * parses the url
 * explodes on the $subdomain_domain_tld['path']
 * returns an array of the cURLed results page and the directory path
 * 
 * @param string $url
 * @return array 
 */
function curl_page_explode_url($url){
    
    $results_page = myCurl($url);
    

    //remove http://subdomain.domain.tld
    $subdomain_domain_tld = parse_url($url);
    
    $results_array = [];
    if(!empty($subdomain_domain_tld))
    {    
        //break relative url on / (get folders and file)
        if(array_key_exists('path', $subdomain_domain_tld))
        {
            $dir_path = explode('/',"{$subdomain_domain_tld['path']}");
        }
        else
        {
            $dir_path = explode('/',"{$subdomain_domain_tld['host']}");
        }
        $results_array = [$results_page, $dir_path];
    } 

    return $results_array;
}
//going through all the urls one by one:
//create a directory that matches the subdomain of the url and initialize a git repo
// with each dir in the url path being created
//using a regular expression, search the index page for any assets. 
//retireve assets
//one asset at a time create a dir for each value in the url path then save the asset to file in that path relative to the index.html path
//if it is a .css file, repeat: search for assets, retrieve them, create directory path, then save to file.
//once one full subdomain cycle is done, add and commit those changes in that repository
//repeats for every given key in the subdomain array

