<?php

$view_id = '45013658';

error_reporting(E_ALL);
//ini_set('display_errors','1');

$day = date('Y-m-d');
if(isset($_GET['day']))
{
        $day = $_GET['day'];
}

//start GA stuff
require_once('../rg_dev/google-api-php-client/src/Google/autoload.php');
require_once('../rg_dev/google-api-php-client/src/Google/Client.php');
require_once('../rg_dev/google-api-php-client/src/Google/Service/Analytics.php');
require_once('scraper_functions.php');

$client = new Google_Client();
$client->setApplicationName("CT GA Dashboards");

$client->setAssertionCredentials(
        new Google_Auth_AssertionCredentials(
                "771931437620-0bmrknov97srjhdah5rpmutoj8i60t9n@developer.gserviceaccount.com", //app email
                array('https://www.googleapis.com/auth/analytics.readonly'),
                file_get_contents('http://rg.contourthis.com/hidden/CT_GA_Dashboards-794bacce60f0.p12')
        )
);

$client->setClientId('771931437620-0bmrknov97srjhdah5rpmutoj8i60t9n.apps.googleusercontent.com');
$client->setAccessType('offline_access');

$service = new Google_Service_Analytics($client);

$start = $day;
$end = $day;

if(isset($_GET['start'])){
  $start = $_GET['start'];
}
if(isset($_GET['end'])){
  $end = $_GET['end'];
}

$pages= $service->data_ga->get(
        'ga:'.$view_id,
        $start,
        $end,
        'ga:visits',
        array(
                'dimensions' => 'ga:pagePath',
                'metrics' => 'ga:uniquePageviews',
                'max-results' => '10000'
        ));

//$subdomains_array = ga_urls($pages['rows']);
$subdomains_array = array(
    'coaches'=>array(
        "http://coaches.revolutiongolf.com/power-leaks/your-number-1-power-leak.aspx"
    )
);
