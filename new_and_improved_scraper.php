<?php
require_once('url_scraper_source.php');
$results_page = '';

foreach($subdomains_array as $key => $subdomain_specific_array)
{
    echo "<pre> <br>";
    echo "all urls under $key:";
    echo "</pre><br>";
    
    $subdomain_specific_dir = '/opt/bitnami/apache2/htdocs/mobile_survey_fix/' . $key;
    
    if ( !is_dir($subdomain_specific_dir) ) 
    {
        mkdir ($subdomain_specific_dir, 0777);
        chmod($subdomain_specific_dir, 0777);  
        chdir($subdomain_specific_dir);
        exec("git init");
    }
    
    $results_array = '';
    foreach($subdomain_specific_array as $url)
    {
        $results_array = create_host_dir($url, $subdomain_specific_dir);

        if(empty($results_array))
        {
            continue;
        }
        
        $url_explode = scrape_between($url, "//", "/");
        
        $separate_results = results_page_regex($results_array[0]);
        
        foreach($separate_results as $key => $separate_result) 
        {    
            if ($key === 0 || $key === 1)
            {
                continue;
            }
            
            //foreach one in the list, get url
            foreach($separate_result as $result)
            {
                if($result === "")
                {
                    continue;
                }
               
                $result_url = add_http($result, $url_explode);
               
                $separate_results_array = curl_page_explode_url($result_url);
                
                $check_if_array_empty = array_filter($separate_results_array);
                
                if(!empty($check_if_array_empty)) 
                {
                    /**
                     * $separate_results_array[1] = the directory path
                     * $separate_results_array[0] is the asset file
                     */
                    $asset_array = create_dir_structure($separate_results_array[1], $separate_results_array[0], $results_array[1]);
                }
                
                //if is css file, parse for images
                if(strpos($asset_array[0], '.css')) 
                { 
                    $css_images = results_page_regex($asset_array[1]);
                   
                    foreach($css_images[4] as $image)
                    {
                        $image_url = add_http($image, $url_explode, $asset_array[2]);
                        $image_array = curl_page_explode_url($image_url);
                        $check_if_array_empty = array_filter($image_array);
                
                        if(!empty($check_if_array_empty)) 
                        {   
                            /**
                            * $image_array[1] = the directory path
                            * $image_array[0] is the asset file
                            */
                            $image_asset_path = create_dir_structure($image_array[1], $image_array[0], $results_array[1]);
                        }
                    }
                }
            }
        }
    }
    
    chdir($subdomain_specific_dir);
    exec("git add -A");  
    exec("git commit -m'Commiting changes. -your friendly auto scraper bot'");
    echo '<br>committed changes to repo<br>';
}
